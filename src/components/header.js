import React from 'react';

class Header extends React.Component{

    state = {
        "keywords":"hello"
    }

    inputChangeHandler = (event) =>{

        this.setState(
            {
                "keywords": event.target.value
            }
        );

    }

    render(){
        return (
            <div>
                <h2>LOGO</h2>
                <input type="text" onChange={this.inputChangeHandler} />
                <div>{this.state.keywords}</div>
            </div>
        )
    }
}

export default Header;