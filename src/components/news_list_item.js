import React from 'react';
import { css } from 'glamor';

const NewsListItem = (props) =>{

    let news_item = css({
        padding:'20px',
        boxSizing:'border-box',
        borderBottom:'1px solid black',
        background:'lightgrey',
        
    })

    let news_item_title = css({
        ':hover':{
            color:'red'
        },
        '@media(max-width:500px)':{
            color:'blue'
        }
    })
    return(
        <div {...news_item}>
        <h3 {...news_item_title}>{props.NewsItem.title}</h3>
        <div>{props.NewsItem.feed}</div>
        </div>
    )
}

export default NewsListItem;