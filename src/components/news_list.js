import React from 'react';

//components
import NewsListItem from './news_list_item';

const NewsList = (props) => {
    
    const items = props.news.map((item)=>{
        return (
            <NewsListItem key={item.id} NewsItem={item}/>
        )
    });

    return(
        <div>
        {props.children}
        {items}
        </div>
    )
}

export default NewsList;